using System.IO;
using RS30Conf.Droid.Modules;
using RS30Conf.Interfaces;
using Xamarin.Forms;
using System;
using Android.Content;
using System.Collections.Generic;
using Android.OS;


[assembly: Dependency (typeof(CSVFileHandler))]

namespace RS30Conf.Droid.Modules
{
	public class CSVFileHandler : ICsvFileHandler
	{
		public void Write (string filename, string content)
		{
            // Create or get the directory where we want to write or file (has to be on externalstorage to be able to send it by mail (the mail application must have the read access)
			var dir = new Java.IO.File (Android.OS.Environment.ExternalStorageDirectory.AbsolutePath + "/RS30InventoryApp/");
			if (!dir.Exists ())
				dir.Mkdirs ();

            // Write the file
			using (System.IO.StreamWriter writer = new System.IO.StreamWriter (dir.AbsolutePath + "/" + filename, false))
			{
				writer.Write (content);
			}

		}

		public void ShareByMail (string filename)
		{
            // First Get the file 
			var dir = new Java.IO.File (Android.OS.Environment.ExternalStorageDirectory.AbsolutePath + "/RS30InventoryApp/");
			var file = new Java.IO.File (dir.AbsolutePath + "/" + filename);

            // Create a new intent with a send action.
			var email = new Intent (Intent.ActionSend);
			email.SetType ("text/CSV");
            // Set the subject
			email.PutExtra (Intent.ExtraSubject, "Inventory CSV File");
            // Set the file as readeable 
            file.SetReadable (true, false);

            // Get the file address
			var uri = Android.Net.Uri.FromFile (file);

            // Add the file to the intent to be sent as an attached file.
			email.PutExtra (Intent.ExtraStream, uri);

            // Set the type of the intent
			email.SetType ("message/rfc822");

            // Load the intent
			Forms.Context.StartActivity (Intent.CreateChooser (email, "Email"));
		}
	}
}
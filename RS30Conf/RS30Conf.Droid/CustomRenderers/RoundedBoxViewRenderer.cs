﻿using System;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using Xamarin.Forms;
using RS30Conf;
using RS30Conf.Droid;
using Android.Util;

[assembly: ExportRendererAttribute (typeof(RoundedBoxView), typeof(RoundedBoxViewRenderer))]
namespace RS30Conf.Droid
{
	public class RoundedBoxViewRenderer : BoxRenderer
	{
		public RoundedBoxViewRenderer ()
		{
			this.SetWillNotDraw (false);
		}

		public override void Draw (Canvas canvas)
		{
			RoundedBoxView rbv = (RoundedBoxView)this.Element;

			Rect rc = new Rect ();
			GetDrawingRect (rc);

			Rect interior = rc;
			interior.Inset ((int)rbv.StrokeThickness, (int)rbv.StrokeThickness);

			Paint p = new Paint () {
				Color = rbv.Color.ToAndroid (),
				AntiAlias = true,
			};

			canvas.DrawRoundRect (new RectF (interior), (float)DpToPx (rbv.CornerRadius), (float)DpToPx (rbv.CornerRadius), p);

			p.Color = rbv.Stroke.ToAndroid ();
			p.StrokeWidth = (float)rbv.StrokeThickness;
			p.SetStyle (Paint.Style.Stroke);

			canvas.DrawRoundRect (new RectF (rc), (float)rbv.CornerRadius, (float)rbv.CornerRadius, p);
		}

		public double DpToPx (double dp)
		{
			DisplayMetrics displayMetrics = Context.ApplicationContext.Resources.DisplayMetrics;
			double px = Math.Round (dp * (displayMetrics.Xdpi / (float)DisplayMetricsDensity.Default));       
			return px;
		}


	}
}


﻿using System;

namespace RS30Conf.Droid
{
	public interface IBarcodeReceiver
	{
		void Result (string result);

		void ReaderConnected ();
	}
}


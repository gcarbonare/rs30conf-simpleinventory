﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using RS30Conf.Droid.RS30Integration;
using RS30Conf.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Graphics.Drawables;

namespace RS30Conf.Droid
{
	[Activity (Theme = "@style/MyTheme", Label = "RS30Conf", Icon = "@drawable/ic_launcher", MainLauncher = true,
		ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			Forms.Init (this, bundle);
			LoadApplication (new App ());

			// Initialize the Reader
			DependencyService.Get<IScannerService> ().InitBarcodeReader ();
		}
	}
}
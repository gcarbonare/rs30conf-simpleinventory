﻿using System;
using RS30Conf.Interfaces;
using System.Runtime.CompilerServices;
using ZXing.Mobile;
using Xamarin.Forms;
using RS30Conf.IOS;
using Foundation;
using RS30Conf.Common;
using System.Collections.Generic;
using CoreBluetooth;
using ZXing;
using RS30Conf.Models;


[assembly: Xamarin.Forms.Dependency (typeof(ZXingScannerService))]
namespace RS30Conf.IOS
{
	public class ZXingScannerService : IScannerService
	{
		ZXing.Mobile.MobileBarcodeScanner scanner;
		MobileBarcodeScanningOptions scannerOptions;

		#region IScannerService implementation

		public event EventHandler<BarcodeEventArgs> BarcodeReceived;

		#region InitBarcodeReader

		public void InitBarcodeReader ()
		{
            // Initialize the scanner and set it up
			scanner = new MobileBarcodeScanner ();
			scannerOptions = new MobileBarcodeScanningOptions ();
			SetupBarcodeReader ();
		}

		#endregion

		#region SetupBarcodeReader

		public void SetupBarcodeReader ()
		{
            // Setup the scanner options
			scannerOptions.AutoRotate = false;
			scannerOptions.PossibleFormats = new List<ZXing.BarcodeFormat> () {
				BarcodeFormat.CODE_128, 
				BarcodeFormat.CODE_39, 
				BarcodeFormat.EAN_13, 
				BarcodeFormat.DATA_MATRIX,
				BarcodeFormat.EAN_8,
				BarcodeFormat.QR_CODE
			};
		}

		#endregion

		#region ReadBarcode

		public async void ReadBarcode ()
		{
            // Start the scanner
			scanner = new MobileBarcodeScanner ();
			var result = await scanner.Scan (scannerOptions);
			if (result != null)
			{
                // if the result isn't null nor the event handler
				if (BarcodeReceived != null)
				{
                    // Then we fire the BarcodeReceivedEvent
					BarcodeReceived (this, new BarcodeEventArgs (result.Text));
				}

			}
		}

		#endregion

		#region ReleaseBarcodeReader

		public void ReleaseBarcodeReader ()
		{
            // there no need here to release the barcode
		}

		#endregion

		#region ToggleKeyboardEmulation

		public void ToggleKeyboardEmulation (bool isEnabled)
		{
			// there's no keyboard emulation 
		}

		#endregion

		#region ToggleQRCodeReadability

		public void ToggleQRCodeReadability (bool canRead)
		{
			if (canRead)
			{
				if (!scannerOptions.PossibleFormats.Contains (ZXing.BarcodeFormat.QR_CODE))
				{
					scannerOptions.PossibleFormats.Add (ZXing.BarcodeFormat.QR_CODE);
				}
			}
			else
			{
				if (scannerOptions.PossibleFormats.Contains (ZXing.BarcodeFormat.QR_CODE))
				{
					scannerOptions.PossibleFormats.Remove (ZXing.BarcodeFormat.QR_CODE);
				}
			}
		}

		#endregion

		#region ToggleEAN13Readability

		public void ToggleEAN13Readability (bool canRead)
		{
			if (scannerOptions == null)
				scannerOptions = new MobileBarcodeScanningOptions ();
			if (canRead)
			{
				if (!scannerOptions.PossibleFormats.Contains (ZXing.BarcodeFormat.EAN_13))
				{
					scannerOptions.PossibleFormats.Add (ZXing.BarcodeFormat.EAN_13);
				}
			}
			else
			{
				if (scannerOptions.PossibleFormats.Contains (ZXing.BarcodeFormat.EAN_13))
				{
					scannerOptions.PossibleFormats.Remove (ZXing.BarcodeFormat.EAN_13);
				}
			}
		}

		#endregion

		#endregion
	}
}


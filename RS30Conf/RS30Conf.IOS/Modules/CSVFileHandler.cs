﻿using System;
using RS30Conf.Interfaces;
using Xamarin.Forms;
using RS30Conf.IOS;
using MessageUI;
using System.Net.Mail;
using Foundation;
using UIKit;
using System.IO;

[assembly: Dependency (typeof(CSVFileHandler))]
namespace RS30Conf.IOS
{
	public class CSVFileHandler : ICsvFileHandler
	{
		public CSVFileHandler ()
		{
		}

		#region ICsvFileHandler implementation

		public void Write (string filename, string content)
		{
            // Write the file on the device
			var documents =	Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments); 
			var fullPath = Path.Combine (documents, filename);
			File.WriteAllText (fullPath, content);
		}

		public void ShareByMail (string filename)
		{
            // Get the file we want to send 
			var documents =	Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments); 
			var fullPath = Path.Combine (documents, filename);
            // Create the controller that we'll use to send our mail
			var controller = new MFMailComposeViewController ();

            // Set the subject
			controller.SetSubject ("Inventory CSV");
            // Add the file as attached file
			controller.AddAttachmentData (NSData.FromFile (fullPath), "text/CSV", filename);

            // Set the Finished event handler that will dismiss the view.
			controller.Finished += (object sender, MFComposeResultEventArgs e) =>
			{
				e.Controller.DismissViewController (true, null);
			};

			// Get access to the active UIWindow of your application
			var window = UIApplication.SharedApplication.KeyWindow;
            // Get the RootViewController of this UIWindow
			var vc = window.RootViewController;
            // Cast it as a navication controller
			var navcontroller = vc as UINavigationController;
			if (navcontroller != null)
				vc = navcontroller.VisibleViewController;
            // Present the mailcomposerviewcontroller
			vc.PresentViewController (controller, true, null);

		}

		#endregion
	}
}


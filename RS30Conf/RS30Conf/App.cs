﻿using RS30Conf.Views;
using Xamarin.Forms;

namespace RS30Conf
{
	public class App : Application
	{
		public App ()
		{
			// The root page of your application
			MainPage = new NavigationPage (new InventoryPage ());
			RegisterStyles ();
		}

		void RegisterStyles ()
		{
			Current.Resources = new ResourceDictionary ();

			//Style for Labels 
			var labelStyle = new Style (typeof(Label)) {
				Setters = { new Setter { Property = Label.TextProperty, Value = ColorResources.TextColor } 
				}
			};

			Current.Resources.Add (labelStyle);

			var buttonStyle = new Style (typeof(Button)) {
				Setters = {
					new Setter { Property = VisualElement.BackgroundColorProperty, Value = ColorResources.PrimaryColor },
					new Setter { Property = Button.TextColorProperty, Value = ColorResources.ButtonTextColor },
					new Setter { Property = Button.BorderRadiusProperty, Value = 21 },
					new Setter { Property = Button.FontSizeProperty, Value = "16sp" },
					new Setter { Property = VisualElement.HeightRequestProperty, Value = 42 },
					//new Setter { Property = Button.BorderWidthProperty, Value = 2.5},
				}
			};
			Current.Resources.Add (buttonStyle);
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
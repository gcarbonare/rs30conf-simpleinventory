﻿using System;
using System.Net.Http;
using RS30Conf;

namespace RS30Conf
{
	public class BarcodeEventArgs : EventArgs
	{
		public BarcodeEventArgs (string message)
		{
			barcodeMessage = message;
		}

		private readonly string barcodeMessage;

		public string BarcodeMessage {	get { return barcodeMessage; } }
	}

	public delegate void BarcodeMessageEventHandler (object sender, BarcodeEventArgs args);
}


﻿using Xamarin.Forms;
using System.Net;

namespace RS30Conf.Common
{

    // Really simple AppSettings class
    // A better implementation would be to save these settings into some local file/database
	public static class AppSettings
	{
		public static bool CanReadQRCode { get; set; }

		public static bool CanReadEAN13 { get; set; }

		public static bool IsKeyboardEmulationEnabled { get; set; }

	}

}
﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using RS30Conf.Models;

namespace RS30Conf.Common
{
	public class BarcodeCsvGenerator
	{
		private readonly string _separatorCharacter;

		public BarcodeCsvGenerator ()
		{
            // Set the separator character wanted for the csv generation
		    _separatorCharacter = ";";
		}

		private string GenerateFirstLine ()
		{

            // Generates first line for the CSV with the names of the Barcode Entity Property
			var firstLine = "";

			firstLine += "Barcode" + _separatorCharacter;
			firstLine += "Quantity" + _separatorCharacter;
			firstLine += "\r\n";

			return firstLine;
		}

	    /// <summary>
	    /// Generates a CSV Formatted string
	    /// </summary>
	    /// <returns>Return the CSV Formatted string</returns>
	    public string ToCSVString(IEnumerable<Barcode> barcodes)
	    {
	        var contentCsv = GenerateFirstLine();

	        foreach (var item in barcodes)
	        {
	            contentCsv += item.DecodedBarcode + _separatorCharacter;
	            contentCsv += item.Quantity + _separatorCharacter;
	            contentCsv += "\r\n";
	        }

	        return contentCsv;
	    }
	}
}
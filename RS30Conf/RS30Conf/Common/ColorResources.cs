﻿using System;
using System.Diagnostics.Contracts;
using Xamarin.Forms;

namespace RS30Conf
{
	public static class ColorResources
	{
		public static Color BackgroundColor = Color.White;
		public static Color PrimaryColor = Color.FromHex ("#2196F3");
		public static Color PrimaryDarkColor = Color.FromHex ("#1976D2");
		public static Color PrimaryLightColor = Color.FromHex ("#BBDEFB");
		public static Color AccentColor = Color.FromHex ("#03A9F4");
		public static Color PrimaryTextColor = Color.FromHex ("#212121");
		public static Color SecondaryTextColor = Color.FromHex ("#727272");
		public static Color DividerColor = Color.FromHex ("#B6B6B6");
		public static Color TextColor = Color.Black;
		public static Color ButtonTextColor = Color.White;
		public static Color IconColor = Color.White;
	}
}



﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using RS30Conf.Common;
using RS30Conf.Interfaces;
using RS30Conf.Models;
using Xamarin.Forms;

namespace RS30Conf.Views
{
	public partial class InventoryPage : ContentPage
	{
		public InventoryPage ()
		{
			// This must be called to display what have been written in XAML.
			InitializeComponent ();
			BarCodes = new ObservableCollection<Barcode> ();

			// Set standard settings
			AppSettings.CanReadEAN13 = true;
			AppSettings.CanReadQRCode = true;
			AppSettings.IsKeyboardEmulationEnabled = false;

			// Subscribe to the ScannerService BarcodeReceived event.
			DependencyService.Get<IScannerService> ().BarcodeReceived += OnBarcodeReceived;
		}


		public ObservableCollection<Barcode> BarCodes { get; set; }

		/// <summary>
		/// Executed each time the page is displayed on the screen.
		/// </summary>
		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			// We make the KeyboardInputLayout only if the keyboard emulation is enabled on the device
			this.KeyboardInputLayout.IsVisible = AppSettings.IsKeyboardEmulationEnabled;
		}

		/// <summary>
		/// Handle Barcode decoded event messages
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		private void OnBarcodeReceived (object sender, BarcodeEventArgs args)
		{
			string message = args.BarcodeMessage;
			// Remove any newline character in case we are using Keyboard Emulation
			message = message.Replace (Environment.NewLine, string.Empty);
        
			//We check if the barcodes already exist in our saved list
			var itemInList = BarCodes.SingleOrDefault (p => p.DecodedBarcode == message);

			if (itemInList == null)
			{
				// If it doesn't exist we add it to the list
				BarCodes.Add (new Barcode {
					DecodedBarcode = message,
					Quantity = 1
				});
			}
			else
			{
				// If it exist we increment the quantity
				itemInList.Quantity++;
			}

			// Refresh the source of the ListView
			ListInventory.ItemsSource = null;
			ListInventory.ItemsSource = BarCodes;
		}

		private void SettingsButtonClicked (object sender, EventArgs e)
		{
			// Display the SettingsPage
			Navigation.PushAsync (new SettingsPage ());
		}

		private void ShareByMailButtonClicked (object sender, EventArgs e)
		{
			//If we are finished, export, and sendmail
			var csvContent = new BarcodeCsvGenerator ().ToCSVString (BarCodes);
			// Write the CSV File 
			DependencyService.Get<ICsvFileHandler> ().Write ("ExportedRS30.csv", csvContent); 
			// Send it by Email
			DependencyService.Get<ICsvFileHandler> ().ShareByMail ("ExportedRS30.csv"); 
		}

		private void ScanButtonClicked (object sender, EventArgs e)
		{
			// Start barcode reading
			DependencyService.Get<IScannerService> ().ReadBarcode ();
		}


		/// <summary>
		/// Raised when the enter key is pressed(or emulated) while focus is on  KeyboardEmulationEntry
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnKeyboardEmulationEntryCompleted (object sender, EventArgs e)
		{
			// Check if the entry is empty or not
			if (!string.IsNullOrWhiteSpace (KeyboardEmulationEntry.Text))
			{
				// Fire the OnBarcodeReceivedEvent
				OnBarcodeReceived (this, new BarcodeEventArgs (KeyboardEmulationEntry.Text));
				KeyboardEmulationEntry.Text = string.Empty;
			}
		}



	}
}
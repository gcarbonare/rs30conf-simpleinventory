﻿using System;
using RS30Conf.Common;
using RS30Conf.Interfaces;
using Xamarin.Forms;

namespace RS30Conf.Views
{
	public partial class SettingsPage : ContentPage
	{
		public SettingsPage ()
		{
			InitializeComponent ();

            // Update the UI reflecting the settings 
			ScanEAN13Switch.IsToggled = AppSettings.CanReadEAN13;
			ScanQRCodeSwitch.IsToggled = AppSettings.CanReadQRCode;
			KeyboardEmulationSwitch.IsToggled = AppSettings.IsKeyboardEmulationEnabled;
		}



		private void OnScanEAN13SwitchToggled (object sender, ToggledEventArgs e)
		{
            // Change settings
			AppSettings.CanReadEAN13 = ((Switch)sender).IsToggled;
            // Call the ScannerService to apply the new setting.
			DependencyService.Get<IScannerService> ().ToggleEAN13Readability (AppSettings.CanReadEAN13);
		}

		private void OnScanQRCodeSwitchToggled (object sender, ToggledEventArgs e)
		{
			AppSettings.CanReadQRCode = ((Switch)sender).IsToggled;
			DependencyService.Get<IScannerService> ().ToggleQRCodeReadability (AppSettings.CanReadQRCode);
		}


		private void OnKeyboardEmulationSwitchToggled (object sender, ToggledEventArgs e)
		{
			AppSettings.IsKeyboardEmulationEnabled = ((Switch)sender).IsToggled;
			DependencyService.Get<IScannerService> ().ToggleKeyboardEmulation (AppSettings.IsKeyboardEmulationEnabled);
		}

	}
}
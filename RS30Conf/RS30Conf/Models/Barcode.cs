﻿namespace RS30Conf.Models
{
	public class Barcode
	{
		public string DecodedBarcode { get; set; }

		public int Quantity { get; set; }
	}
}
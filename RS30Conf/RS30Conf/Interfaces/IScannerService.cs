﻿using System;
using RS30Conf.Common;

namespace RS30Conf.Interfaces
{
	public interface IScannerService
	{
		void InitBarcodeReader ();

		void SetupBarcodeReader ();

		void ToggleKeyboardEmulation (bool enable);

		void ToggleQRCodeReadability (bool canRead);

		void ToggleEAN13Readability (bool canRead);

		void ReadBarcode ();

		void ReleaseBarcodeReader ();

		event EventHandler<BarcodeEventArgs> BarcodeReceived;
	}
}
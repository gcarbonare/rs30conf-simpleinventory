﻿namespace RS30Conf.Interfaces
{
	public interface ICsvFileHandler
	{
		void Write (string filename, string content);

		void ShareByMail (string filename);
	}
}